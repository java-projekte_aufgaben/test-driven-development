package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class TestVorstellung {

    private KinoSaal kinoSaal;
    private Vorstellung vorstellung;
    private Ticket ticket;

    @BeforeEach
    void setup() {
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 20);
        kinoSaal = new KinoSaal("Apollo", map);
        vorstellung = new Vorstellung(kinoSaal, Zeitfenster.ABEND, LocalDate.now(), "Uncharted", 10.00F);
        ticket = new Ticket("Apollo", Zeitfenster.ABEND, LocalDate.of(2022, 03, 27), 'A', 10);
    }

    @Test
    void testGetFilm() {
        assertEquals("Uncharted", vorstellung.getFilm(), "The assert should answer with true");
        assertNotEquals("The Batman", vorstellung.getFilm(), "The assert should answer false");
    }

    @Test
    void testGetSaal() {
        assertEquals(kinoSaal, vorstellung.getSaal(), "The assert should answer with true");
        assertNotEquals(new KinoSaal("MaxDome", Collections.emptyMap()), vorstellung.getSaal(), "The assert should answer false");
    }

    @Test
    void testGetZeitfenster() {
        assertEquals(Zeitfenster.ABEND, vorstellung.getZeitfenster(), "The assert should answer with true");
        assertNotEquals(Zeitfenster.NACHMITTAG, vorstellung.getZeitfenster(), "The assert should answer with false");
    }

    @Test
    void testGetDatum() {
        assertEquals(LocalDate.now(), vorstellung.getDatum(), "The assert should answer with true");
        assertNotEquals(LocalDate.of(2022, 03, 01), vorstellung.getDatum(), "The assert should answer with false");
    }

    @Test
    void testKaufeTicket() {
        Ticket ticket = vorstellung.kaufeTicket('B', 10, 100);
        assertNotNull(vorstellung.kaufeTicket('A', 10, 20), "The assert should answer with true");
        assertThrows(IllegalArgumentException.class, () -> vorstellung.kaufeTicket('Z', 50, 100));
        assertThrows(IllegalStateException.class, () -> vorstellung.kaufeTicket('B', 10, 50), "The assert should throw an Exception (double payment)");
        assertThrows(IllegalArgumentException.class, () -> vorstellung.kaufeTicket('C', 15, 1), "Nicht ausreichend Geld.");
    }

    @ParameterizedTest
    @ValueSource(chars = {'A', 'B', 'C'})
    void testMultipleTicketSales(Character a) {
        Ticket ticket = vorstellung.kaufeTicket(a, 10, 50);
        assertNotNull(ticket);
        System.out.println(ticket);
    }

    @Test
    void testEquals() {
        Vorstellung vorstellung2 = new Vorstellung(kinoSaal, Zeitfenster.NACHMITTAG, LocalDate.now(), "HOHO", 100);
        assertTrue(vorstellung.equals(vorstellung), "The assert should answer with true");
        assertFalse(vorstellung.equals(vorstellung2), "The assert should answer with false");
    }
}
