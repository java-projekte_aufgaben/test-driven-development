package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


public class TestKinoSaal {

    private KinoSaal kinoSaal;

    @BeforeEach
    void setup(){
        //Saal anlegen
        Map<Character, Integer> map = new HashMap<>();
        map.put('A', 10);
        map.put('B', 10);
        map.put('C', 20);
        kinoSaal = new KinoSaal("Apollo", map);
    }

    @Test
    void prüfePlatzTest(){
        assertFalse(kinoSaal.pruefePlatz('A', 11),"The assert should answer with false");
        assertTrue(kinoSaal.pruefePlatz('A', 10),"The assert should answer with true");
        assertTrue(kinoSaal.pruefePlatz('B',10), "The assert should answer with true");
        assertTrue(kinoSaal.pruefePlatz('C',20), "The assert should answer with true");
    }

    @Test
    void getNameTest(){
        assertEquals("Apollo", kinoSaal.getName(),"The assert should answer with true");
        assertNotEquals("MaxDome", kinoSaal.getName(), "The assert should with false");
    }

    @Test
    void equalsTest(){
        Map<Character, Integer> map = new HashMap<>();
        map.put('A',11);
        map.put('B',15);
        KinoSaal kinoSaal1 = new KinoSaal("MaxDome", map);
        KinoSaal kinoSaal2 = new KinoSaal("MaxDome", map);

        assertFalse(kinoSaal.equals(kinoSaal1), "The assert should answer with false");
        assertTrue(kinoSaal2.equals(kinoSaal1), "The assert should aswer with true");
    }

}
