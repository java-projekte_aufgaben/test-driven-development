package at.itkolleg.ase.tdd.kino;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class TestKinoverwaltung {

    KinoVerwaltung kinoVerwaltung;
    private KinoSaal kinoSaal;
    private Vorstellung vorstellung;
    private Ticket ticket;

    @BeforeEach
    void setup(){
        Map<Character,Integer> map = new HashMap<>();
        map.put('A',10);
        map.put('B',10);
        map.put('C',20);
        kinoSaal = new KinoSaal("Apollo", map);
        vorstellung = new Vorstellung(kinoSaal,Zeitfenster.ABEND, LocalDate.now(),"Uncharted",10.00F);
        ticket = new Ticket("Apollo",Zeitfenster.ABEND,LocalDate.of(2022,03,27),'A',10);
        kinoVerwaltung = new KinoVerwaltung();
    }

    @Test
    void testGetVorstellungen(){
        assertTrue(kinoVerwaltung.getVorstellungen().isEmpty());
        kinoVerwaltung.einplanenVorstellung(vorstellung);
        assertFalse(kinoVerwaltung.getVorstellungen().isEmpty());
    }
    @Test
    void testEinplanenVorstellung(){
        kinoVerwaltung.einplanenVorstellung(vorstellung);
        kinoVerwaltung.einplanenVorstellung(new Vorstellung(kinoSaal,Zeitfenster.NACHT,LocalDate.of(2022,03,27),"Morbius",14.99F));
        assertThrows(IllegalArgumentException.class,()->kinoVerwaltung.einplanenVorstellung(vorstellung));
        assertFalse(kinoVerwaltung.getVorstellungen().isEmpty());
        assertEquals(2, kinoVerwaltung.getVorstellungen().size());
    }
    @Test
    void testKaufeTicket(){
        Ticket ticket= kinoVerwaltung.kaufeTicket(vorstellung,'A',10,50);
        assertNotNull(ticket,"The assert should return true");
        assertThrows(IllegalStateException.class,()->kinoVerwaltung.kaufeTicket(vorstellung,'A',10,50));
    }
}
