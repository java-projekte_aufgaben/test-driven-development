package at.itkolleg.ase.tdd.kino;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Dieses Beispiel stammt aus https://training.cherriz.de/cherriz-training/1.0.0/testen/junit5.html
 */
public class App 
{
    public static void main( String[] args )
    {
        //Saal anlegen
        Map<Character,Integer> map = new HashMap<>();
        map.put('A',10);
        map.put('B',10);
        map.put('C',15);
        KinoSaal ks = new KinoSaal("LadyX",map);

        //Platz prüfen
        System.out.println(ks.pruefePlatz('A',11));
        System.out.println(ks.pruefePlatz('A',10));
        System.out.println(ks.pruefePlatz('B',10));
        System.out.println(ks.pruefePlatz('C',14));

        //Zweiten Saal anlegen
        Map<Character, Integer> map2 = new HashMap<>();
        map2.put('A',25);
        map2.put('B',25);
        map2.put('C',30);
        KinoSaal ks2 = new KinoSaal("Apollo", map2);

        //Platz prüfen
        System.out.println(ks2.pruefePlatz('A', 15));
        System.out.println(ks2.pruefePlatz('B', 20));
        System.out.println(ks2.pruefePlatz('B', 28));
        System.out.println(ks2.pruefePlatz('C', 27));

        //Vorstellung anlegen
        Vorstellung vorstellung1 = new Vorstellung(ks,Zeitfenster.ABEND, LocalDate.of(2022,03,27),"Jackass Forever",12.50F);
        Vorstellung vorstellung2 = new Vorstellung(ks2, Zeitfenster.ABEND, LocalDate.of(2022,03,27), "Uncharted",10.00F);
        Vorstellung vorstellung3 = new Vorstellung(ks2,Zeitfenster.NACHT, LocalDate.of(2022,03,27),"Morbius",14.99F);

        //Vorstellung über Kinoverwaltung planen
        KinoVerwaltung kinoVerwaltung = new KinoVerwaltung();
        kinoVerwaltung.einplanenVorstellung(vorstellung1);
        kinoVerwaltung.einplanenVorstellung(vorstellung2);
        kinoVerwaltung.einplanenVorstellung(vorstellung3);

        //Ticket erstellen
        Ticket ticketFürV1 = new Ticket(ks.getName(),Zeitfenster.ABEND,LocalDate.of(2022,03,27),'A',5);
        Ticket ticketFürV2 = new Ticket(ks2.getName(),Zeitfenster.ABEND,LocalDate.of(2022,03,27), 'A',10);

        //Ticket für Vorstellung ausgeben
        System.out.println(ticketFürV1);
        System.out.println(ticketFürV2);

    }
}
