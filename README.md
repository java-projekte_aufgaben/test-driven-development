# Test-Driven-Development TDD

# Inhaltsverzeichnis 

- [Test-Driven-Development TDD](#test-driven-development-tdd)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Aufgabe 1: Theorie Test-Driven-Development TDD](#aufgabe-1-theorie-test-driven-development-tdd)
- [Kent Beck](#kent-beck)
- [Wie funktioniert Test Driven Development genau?](#wie-funktioniert-test-driven-development-genau)
- [Red-Green-Refactor](#red-green-refactor)
- [FIRST-Acronym](#first-acronym)
- [Testarten](#testarten)
  - [Unit-Tests](#unit-tests)
  - [Eignung](#eignung)
  - [Integrationstests](#integrationstests)
  - [Akzeptanztests](#akzeptanztests)
  - [UI - Tests](#ui---tests)
  - [End-to-End (E2E)-Tests](#end-to-end-e2e-tests)
- [Testpyramide](#testpyramide)
- [JUNIT Junit5](#junit-junit5)
- [Mockito](#mockito)
- [Aufgabe 2: Ausgangsprojekt](#aufgabe-2-ausgangsprojekt)
  - [Junit5-Tests](#junit5-tests)


# Aufgabe 1: Theorie Test-Driven-Development TDD 

Mit steigender Komplexität von Computerprogrammen werden auch die **agilen** Methoden des Test Driven Developments (TDD) immer beliebter und das aus gutem Grund. Mit TDD sorgen Programmierer dafür, dass das Design einer Software auch gut durchdacht ist, bevor sie sich ans Schreiben des Funktionscodes machen. Diese Vorgehensweise erhöht nicht nur die Qualität der Software maßgeblich, sondern reduziert auch den Wartungsaufwand.

Die testgetriebene Entwicklung kommt u.a. im Extreme Programming zum Einsatz, das sich durch fortlaufende Reviews, Tests, Design und Redesign auszeichnet. Auch das Test Driven Development hält sich an einen **Entwicklungszyklus**, dessen Reihenfolge für eine effektive Umsetzung eingehalten werden muss. 

# Kent Beck 

Verschiedene Testmethoden, die die Qualität einer Software kontrollieren, gibt es schon lange. Bereits zu Beginn der Software-Entwicklung überprüften unabhängige Tester Computerprogramme im Qualitätsmanagement auf ihre Funktionalität. Die eigentliche Entwicklung der Software und die Testverfahren wurden damals noch als unabhängige Prozesse angesehen. Erst mit dem US-amerikanischen Software-Entwickler und Begründer des Extreme Programmings Kent Beck entstand der **Test-First-Ansatz**. Dieser hat den bisherigen Ablauf einfach umgekehrt: Anstatt erst den Quellcode zu schreiben und diesen dann zu testen, schreibt das Entwicklerteam erst die Tests. Dann verwendet es die Testfälle, um den bestmöglichen Code zu schreiben und zu implementieren. 

Auch wenn die testgetriebene Entwicklung einem Laien zunächst kontraintuitiv erscheint, hat sie durchaus ihren Sinn und führt zu besseren Ergebnissen. Während bei herkömmlichen, nachgeschalteten Testverfahren ein Wasserfallmodell oder V-Modell eingesetzt wird, verlaufen die Prozesse beim TDD nämlich in einem Zyklus. Das heißt, dass Sie zuerst Testfälle bestimmen, die häufig fehlschlagen. Dies geschieht mit Absicht, denn im zweiten Schritt verfassen Sie nur so viel Code, wie für das Bestehen der Tests benötigt wird. Daraufhin werden die Komponenten refaktorisiert: Unter Beibehaltung seiner Funktionen erweitern Sie den Quellcode oder strukturieren ihn falls nötig um. 

# Wie funktioniert Test Driven Development genau? 

Das Test Driven Development orientiert sich an den Ergebnissen der von Ihnen bestimmten Testfälle. Die zyklische Verfahrensweise stellt sicher, dass der Code erst ins Produktivsystem übertragen wird, wenn alle Anforderungen an die Software erfüllt sind. Das heißt, dass Sie die Codebestandteile so oft refaktorisieren und neu testen, bis der Test nicht mehr als fehlerhaft angezeigt wird. Durch diese Methode reichern Sie die Software schrittweise mit neuen Funktionen an, da Sie nach jedem bestandenen Test einen neuen Quellcode verfassen. Aus diesem Grund zählt TDD auch zu den **inkrementellen Vorgehensmodellen** in der Software-Entwicklung.

Einzelne Testfälle durchlaufen den Zyklus in der Regel nicht länger als ein paar Sekunden oder Minuten. So machen sich die Resultate schnell im Produktivcode bemerkbar. Damit die Iteration ohne zusätzlichen Aufwand geschieht, brauchen Sie ein TDD-Werkzeug und -Framework. Typischerweise verwenden Entwickler ein Tool zur Build-Automatisierung wie CruiseControl oder Jenkins. Diese erlauben die kontinuierliche und fehlerfreie **Integration von Komponenten** in den Quellcode. Beliebt bei der Java-Entwicklung sind auch JUnit, Maven oder Ant. Generell gilt: Die Tests werden immer in der gleichen Sprache wie der Funktionscode geschrieben. Für PHP gibt es u.a. die Werkzeuge Ceedling oder CMock. 

# Red-Green-Refactor 

Der Zyklus, dem Programmierer im Test Driven Development folgen, wird auch **Red-Green-Refactor-Zyklus** genannt. Dieser beschreibt die einzelnen Phasen, die Sie für maximale Effizienz durchlaufen:

- **Rote Phase**: In dieser Phase denken Sie sich in die Rolle des Nutzers hinein. Dieser möchte den Code auf einfache Weise verwenden. Sie schreiben also einen Test, der Komponenten enthält, die noch nicht implementiert wurden. So müssen Sie eine Entscheidung darüber treffen, welche Elemente für das Funktionieren eines Codes wirklich notwendig sind.
- **Grüne Phase**: Angenommen, der Test schlägt fehl und wird rot markiert. Nun nehmen Sie die Rolle eines Programmierers ein, der versucht, eine simple Lösung zu finden. **Wichtig**: Sie schreiben nur so viel Code wie nötig. Diesen integrieren Sie in den Produktivcode, sodass der Test mit grün markiert wird. 
- **Refactoring**: In diesem Schritt wird der Produktivcode regelrecht „aufgeräumt“ und in seiner Struktur perfektioniert. Das heißt, Sie sollten ihn ergänzen und so umstrukturieren, dass er aus Entwicklersicht elegant und verständlich ist. Entfernen Sie z.B. Code-Duplizierungen und bringen Sie ihn so auf ein professionelles Niveau. 

# FIRST-Acronym 

- **Fast** Unit-Tests sollten schnell sein, da sie sonst die Entwicklungs-/Einsatzzeit verlangsamen und es länger dauert, bis sie bestanden oder fehlgeschlagen sind. Auf einem ausreichend großen System gibt es in der Regel ein paar tausend Unit-Tests - sagen wir 2000 Unit-Tests. Wenn der durchschnittliche Unit-Test 200 Millisekunden für die Ausführung benötigt (was als schnell zu betrachten ist), dann dauert es 6,5 Minuten, um eine komplette Suite auszuführen.
  
  6,5 Minuten scheinen in diesem Stadium nicht lang zu sein, aber stellen Sie sich vor, dass Sie diese Tests mehrmals täglich auf Ihrem Entwicklungscomputer ausführen, was eine ganze Menge produktiver Zeit verschlingt. Und stellen Sie sich vor, dass die Anzahl dieser Tests zunimmt, wenn neue Funktionalitäten zur Anwendung hinzugefügt werden, was die Testausführungszeit weiter erhöht.

    Der Wert Ihrer Unit-Testsuite nimmt ab, da ihre Fähigkeit, kontinuierliches, umfassendes und schnelles Feedback über den Zustand Ihres Systems zu liefern, ebenfalls abnimmt.

    Eine der Hauptursachen für langsame Tests sind Abhängigkeiten, die mit externen, bösen Notwendigkeiten wie Datenbanken, Dateien und Netzwerkaufrufen umgehen müssen. Sie benötigen Tausende von Millisekunden. Um eine Suite schnell zu machen, müssen Sie diese Abhängigkeiten vermeiden, indem Sie Mock-Tests verwenden.

- **Isolated** Schreiben Sie niemals Tests, die von anderen Testfällen abhängen. Egal wie sorgfältig Sie sie entwerfen, es wird immer die Möglichkeit von Fehlalarmen geben. Um die Situation noch schlimmer zu machen, könnten Sie am Ende mehr Zeit damit verbringen, herauszufinden, welcher Test in der Kette den Fehler verursacht hat.

    Im besten Fall sollten Sie in der Lage sein, jeden Test zu jeder Zeit und in jeder Reihenfolge durchzuführen.

    Durch die Erstellung unabhängiger Tests ist es einfach, Ihre Tests nur auf einen kleinen Teil des Verhaltens zu konzentrieren. Wenn dieser Test fehlschlägt, wissen Sie genau, was falsch gelaufen ist und wo. Es ist nicht nötig, den Code selbst zu debuggen.

    Das Single Responsibility Principle (SRP) der SOLID Class-Design Principles besagt, dass Klassen klein und für einen einzigen Zweck bestimmt sein sollten. Dies kann auch auf Ihre Tests angewandt werden. Wenn eine Ihrer Testmethoden aus mehr als einem Grund fehlschlagen kann, sollten Sie in Erwägung ziehen, sie in separate Tests aufzuteilen.

- **Repeatable** Ein wiederholbarer Test ist ein Test, der bei jeder Durchführung die gleichen Ergebnisse liefert. Um wiederholbare Tests durchführen zu können, müssen Sie sie von allen Elementen der externen Umgebung isolieren, die nicht unter Ihrer direkten Kontrolle stehen. In diesen Fällen können Sie gerne Mock-Objekte verwenden. Sie wurden genau für diesen Zweck entwickelt.

    Gelegentlich müssen Sie direkt mit einer externen Umgebung interagieren, z.B. mit einer Datenbank. Sie sollten eine private Sandbox einrichten, um Konflikte mit anderen Entwicklern zu vermeiden, deren Tests gleichzeitig die Datenbank verändern. In dieser Situation können Sie In-Memory-Datenbanken verwenden.

    Wenn Tests nicht wiederholbar sind, werden Sie mit Sicherheit einige falsche Testergebnisse erhalten, und Sie können es sich nicht leisten, Zeit mit der Suche nach Phantomproblemen zu verschwenden. 

- **Self-validating** Tests müssen selbstüberprüfend sein - jeder Test muss in der Lage sein, festzustellen, ob die Ausgabe erwartet wird oder nicht. Er muss feststellen, ob er fehlgeschlagen ist oder bestanden wurde. Es darf keine manuelle Interpretation der Ergebnisse geben.

    Die manuelle Überprüfung der Testergebnisse ist ein zeitaufwändiger Prozess, der auch mehr Risiken mit sich bringen kann.

    Stellen Sie sicher, dass Sie nichts Dummes tun, wie z. B. einen Test so zu gestalten, dass er manuelle Vorbereitungsschritte erfordert, bevor Sie ihn durchführen können. Sie müssen jede Einrichtung, die Ihr Test erfordert, automatisieren - verlassen Sie sich auch nicht auf das Vorhandensein einer Datenbank und vorgefertigter Daten.

    Legen Sie eine In-Memory-Datenbank an, erstellen Sie ein Schema, legen Sie Dummy-Daten ein und testen Sie dann den Code. Auf diese Weise können Sie den Test N-mal ausführen, ohne externe Faktoren befürchten zu müssen, die die Testausführung und ihr Ergebnis beeinflussen könnten. 

- **Timely** Praktischerweise können Sie jederzeit Unit-Tests schreiben. Sie können warten, bis der Code produktionsreif ist, oder Sie konzentrieren sich besser auf das rechtzeitige Schreiben von Unit-Tests.

    Als Vorschlag sollten Sie Richtlinien oder strenge Regeln für Unit-Tests aufstellen. Sie können Überprüfungsprozesse oder sogar automatisierte Tools einsetzen, um Code ohne ausreichende Tests zurückzuweisen.

    Je mehr Sie Unit-Tests durchführen, desto mehr werden Sie feststellen, dass es sich lohnt, kleinere Codeabschnitte zu schreiben, bevor Sie einen entsprechenden Unit-Test in Angriff nehmen. Erstens ist es dann einfacher, den Test zu schreiben, und zweitens zahlt sich der Test sofort aus, wenn Sie den Rest des Verhaltens im umgebenden Code ausarbeiten. 

<br>

# Testarten 

## Unit-Tests 

Ein Unittest (zu Deutsch auch Modultest oder Komponententests) bezeichnet das Testen der einzelnen Bestandteile eines Softwaresystems auf deren korrekte Funktionsweise. Die Tests stellen die granularste Möglichkeit dar Softwarebestandteile zu testen. Damit bilden sie die Basis der Testpyramide. 

Unittests eignen sich hervorragend als Regressionstests. Die Validierungslogik ist fest programmiert und validiert Module auch bei mehrfacher Testausführung immer identisch (idempotent). Dazu gehört es aber auch, den Code so zu schreiben, dass sich dieser gut testen lässt. Das heißt, der getestete Code ist nicht von externen Faktoren wie beispielsweise Datenbanken oder einer Uhrzeit abhängig. Dabei kann es teilweise recht kompliziert sein diese Abhängigkeiten auszuklammern. Generell gilt aber: Gute Testbarkeit ist ein Indikator für hohe Code- / Softwarequalität. 

## Eignung 

Der Einsatz von Unittests ist unbestritten eines der wichtigsten Instrumente zur Entwicklung nachhaltiger Softwaresysteme. Dennoch eignen sich Unittests nicht für jedes Szenario gleichermaßen.

Besonders gut eignen sich Tests…​

- …​ für die Validierung von Komponenten mit einer begrenzten Zahl an Schnittstellen.

- …​ für die Validierung rein technischer Komponenten. Die Tests von Komponenten zur Mensch-Maschine-Interaktion sind deutlich komplexer.

- …​ wenn bereits eine grundlegende Testabdeckung (auch Test-Coverate) für ein System erreicht wurde. Die Testabdeckung beschreibt das Verhältnis aller möglichen Zustände aller Funktionen eines Modules zu denen, für die ein Testfall besteht.

- …​ wenn die Buildinfrastruktur ein automatisiertes Ausführen aller Tests unterstützt.

- …​ wenn eine Continous Integration Pipeline aufgesetzt werden soll. Automatisierte Tests sind die Grundvoraussetzung für einen automatischen Integrationsprozess (dasselbe gilt natürlich auch für Continous Deployment).

Eher ungeeignet sind Unittests bei…​

- …​ Systemen mit einer sehr hohen Schnittstellenanzahl.

- …​ Systemen mit Schnittstellen zur Interaktion mit analogen Schnittstellen (grafische Benutzeroberfläche, Funkübertragung, …​),

- …​ systemübergreifenden Testfällen.

- …​ Modulen deren Funktionsweise sich häufig grundlegend ändert.

- …​ Systemen an denen wenig Änderungen durchgeführt werden. 

## Integrationstests 

Zu wissen dass der eigene Code für sich alleine funktioniert ist ein Anfang. Damit weiss man aber noch nicht ob der Code auch mit anderen Teilen funktioniert. Hier kommen die Integrationstests ins Spiel.

Auf dieser Ebene werden all die Abhängigkeiten angeschaut die man bei den Unit-Tests entfernt hat. Was zuerst nach vermeidbarem Zusatzaufwand aussieht hat sehr wohl seine Berechtigung. Es genügt wenn man das Erzeugen, Speichern, Aktualisieren und Löschen eines Objekts in der Datenbank ein Mal pro Klasse testet. Dies hat die gleiche Aussagekraft (ist aber deutlich schneller) wie wenn man in allen Unit-Tests immer mit den Objekten aus der Datenbank arbeiten würde.

Da weniger Tests mit den Umsystemen nötig sind wirkt sich deren Ausführungsdauer nicht so stark auf die Länge des gesamten Testlaufs aus. 

## Akzeptanztests 

Akzeptanztestgetriebene Entwicklung (ATDD) ist zwar mit testgetriebener Entwicklung verwandt, unterscheidet sich jedoch in der Vorgehensweise von testgetriebener Entwicklung. 

 Vom GUI durch die Geschäftslogik hin zur Datenbank und den externen Webservices soll hier alles geprüft werden.

Akzeptanztestgetriebene Entwicklung ist ein Kommunikationswerkzeug zwischen dem Kunden bzw. den Anwendern, den Entwicklern und den Testern, welches sicherstellen soll, dass die Anforderungen gut beschrieben sind. Akzeptanztestgetriebene Entwicklung verlangt keine Automatisierung der Testfälle, wenngleich diese fürs Regressionstesten hilfreich wäre. Die Tests bei akzeptanztestgetriebener Entwicklung müssen dafür auch für Nicht-Entwickler lesbar sein. Die Tests der testgetriebenen Entwicklung können in vielen Fällen aus den Tests der akzeptanztestgetriebenen Entwicklung abgeleitet werden.
<br>

## UI - Tests

UI - Tests automatisieren sich wiederholende Aufgaben, um sicherzustellen, dass Ihre wichtigsten UI-Interaktionen weiterhin funktionieren, während Sie neue Funktionen hinzufügen oder die Codebasis Ihrer App umgestalten. Es kann auch neuen Entwicklern helfen, die App zu verstehen, da sie den UI-Test nur einmal ausführen müssen, er kann das automatisierte Testskript sehen, auf dem die App ausgeführt wird, und von dort aus die Benutzerreise der App interpretieren.
Um auf das Beispiel einer Anmeldefunktion zurückzukommen, werden UI-Tests dies tun

- Tippen Sie auf den Benutzernamen.
- Geben Sie „Benutzername“ ein.
- Tippen Sie auf Passwort.
- Geben Sie „******“ ein.
- Tippen Sie auf Senden. 
  
Hier simuliert das Skript eine Erfolgs- oder Fehlerantwort, um die App zum nächsten Bildschirm zu verschieben, oder zeigt ein Warnfeld an, wenn ein Fehler auftritt.

## End-to-End (E2E)-Tests 

Genauso wie Integrationstests für Unit-Tests funktionieren, sollten E2E -Tests die Benutzerwege der App testen, indem sie Real Network Response verwenden, indem sie die Real-Server-Endpunkte erreichen.

Das End-to-End-Testing ist eine Methode des Testens, bei der die fachlichen Geschäftsabläufe innerhalb einer Anwendung ganzheitlich verifiziert werden. Bei klassischen Client-Server Applikationen handelt es sich in der Regel um Prüfvorgänge als komplette Round-Trips vom Anfang (Frontend) bis zum Ende (Backend) und zurück zum Frontend. Das Ziel dahinter ist die Sicherung des erwarteten Anwenderflusses und die rechtzeitige Entdeckung von Fehlern aus der Benutzersicht vor dem eigentlichen Release. Aus diesem Grund wird diese Testart auch als Akzeptanztests bezeichnet.  Oftmals ist ein Softwaresystemnetzwerk sehr komplex aufgebaut und von weiteren Subsystemen abhängig. Darauffolgend, wenn Fehler nicht rechtzeitig behoben werden, ist das gesamte System vom Abstürzen oder Fehlverhalten bedroht. Mit End-to-End-Tests lassen sich solche Risiken vermeiden, indem ein reales Anwendungsszenario simuliert wird und das zu testende System mit ihren Sub-Elementen auf Integrierbarkeit und korrektes Zusammenspiel geprüft werden. 

Der größte Vorteil dieser Technik ist die Simulation von User Experience in einer wirklichkeitsgetreuen Umgebung, weil es auf diese Weise mögliche Herausforderungen bei den Benutzern festgestellt werden können. So können Lösungen implementiert werden, die die Qualität der Benutzererfahrung erhöhen und für die langfristige Zufriedenheit sorgen. 

![End-To-End](images/endToEnd.png) 

# Testpyramide 

Um ein System wirklich zu testen gilt es mehrere Ebenen anzuschauen. Die Testpyramide zeigt diese auf und vermittelt auf eine leicht verständliche Weise wie sich die Anzahl der Testfälle staffeln soll. 

![Testpyramide](images/pyramide.png)
<br><br>

# JUNIT Junit5 

JUnit ist ein Framework zum Testen von Java-Programmen, das besonders für automatisierte Unit-Tests einzelner Units (Klassen oder Methoden) geeignet ist.

Ohne JUnit kommt heutzutage kein Java-Projekt und kein Java-Entwickler aus. Das Tool zum automatisierten Testen ist seit knapp 20 Jahren der Industriestandard und nahezu ein Synonym für Testen in Java. 
<br><br>

# Mockito 

Mockito ist ein Mocking Framework, eine JAVA-basierte Bibliothek, die für effektive Komponententests von JAVA-Anwendungen verwendet wird. Mockito wird verwendet, um Schnittstellen zu simulieren, sodass eine Dummy-Funktionalität zu einer Mock-Schnittstelle hinzugefügt werden kann, die in Komponententests verwendet werden kann. 

Mocking ist eine Möglichkeit, die Funktionalität einer Klasse isoliert zu testen. Mocking erfordert keine Datenbankverbindung oder das Lesen einer Eigenschaftendatei oder eines Dateiservers, um eine Funktionalität zu testen. Scheinobjekte mocken den echten Dienst. Ein Pseudo-Objekt gibt Dummy-Daten zurück, die einer ihm zugeführten Dummy-Eingabe entsprechen. 

Mockito erleichtert das nahtlose Erstellen von Scheinobjekten. Es verwendet Java Reflection, um Mock-Objekte für eine bestimmte Schnittstelle zu erstellen. Mock-Objekte sind nichts anderes als Stellvertreter für tatsächliche Implementierungen.

Stellen Sie sich einen Fall von Stock Service vor, der die Preisdetails einer Aktie zurückgibt. Während der Entwicklung kann der eigentliche Bestandsdienst nicht verwendet werden, um Echtzeitdaten zu erhalten. Wir brauchen also eine Dummy-Implementierung des Stock-Service. Mit Mockito ist dies einfach zu realisieren. 
<br><br>

# Aufgabe 2: Ausgangsprojekt 


## Junit5-Tests 
<br>

![Main-Methode](images/main.png) 

![App-Test](images/AppTest.png)t